{
  description = "A stack sampler for your life";

  inputs = {
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "nixpkgs/nixos-unstable";
  };

  outputs = { self, fenix, flake-utils, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        devToolchain = fenix.packages.${system}.stable.toolchain;
      in {
        devShell = pkgs.mkShell {
          nativeBuildInputs = with pkgs; [
            devToolchain
            pkg-config
            openssl
            sqlx-cli
            sqlite-interactive
            sqlite
            cargo-nextest
            just
          ];
          shellHook = ''export CARGO_FEATURE_STD=true'';
        };
      }
    );
}
