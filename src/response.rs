//! Defines the structure of the freeform response to a ping, including how to parse it.
use std::{collections::HashSet, str::FromStr};

use regex::Regex;
use thiserror::Error;

/// A ping response from the user.
///
/// The response can include 'control codes' like `!ibid` or `!all`. These are removed from the string before anything else. The exact behavior around how control codes are remo
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Response {
    /// If true, the response should apply to all pings sent out since the last one responded to.
    pub apply_to_all: bool,
    pub tags: Tags,
}

/// Indicates which tags the response carries.
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Tags {
    /// Tags are explicitly specified.
    Explicit(HashSet<String>),
    /// Use the same tags as in the previous response.
    Ibid,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum ControlCode {
    ApplyToAll,
    Ibid,
}

impl FromStr for ControlCode {
    type Err = ParseResponseError;

    /// Note that this implementation requires the initial !.
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "all" => Ok(Self::ApplyToAll),
            "ibid" => Ok(Self::Ibid),
            _ => Err(Self::Err::UnknownControlCode(s.to_owned())),
        }
    }
}

#[derive(Error, Debug, Clone, PartialEq, Eq)]
pub enum ParseResponseError {
    #[error("ibid must be exactly !ibid")]
    IbidWithOtherTags,
    #[error("unknown control code `{0}`")]
    UnknownControlCode(String),
}

impl FromStr for Response {
    type Err = ParseResponseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let re = Regex::new(r"(\b|^|\s)!(\w+)").unwrap();
        // First, grab out all the control codes and remove them.
        let control_codes: HashSet<ControlCode> = re
            .captures_iter(s)
            .map(|cap| ControlCode::from_str(&cap[2]))
            .collect::<Result<_, _>>()?;
        let apply_to_all = control_codes.contains(&ControlCode::ApplyToAll);

        let s = re.replace_all(s, "").trim().to_owned();
        // Collapse consecutive whitespace.
        let s = Regex::new(r" +")
            .unwrap()
            .replace_all(&s, " ")
            .trim()
            .to_owned();
        let tags: HashSet<String> = s.split(',').map(|s| s.trim().to_owned()).collect();

        if control_codes.contains(&ControlCode::Ibid) {
            if s.is_empty() {
                Ok(Self {
                    apply_to_all,
                    tags: Tags::Ibid,
                })
            } else {
                Err(ParseResponseError::IbidWithOtherTags)
            }
        } else {
            Ok(Self {
                apply_to_all,
                tags: Tags::Explicit(tags),
            })
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use literally::hset;
    use test_case::test_case;

    #[test_case("!ibid", false, Tags::Ibid; "ibid tag")]
    #[test_case(" !all !ibid ", true, Tags::Ibid; "ibid tag with all")]
    #[test_case(" foo, bar,  baz  ", false, Tags::Explicit(hset!["foo", "bar", "baz"]); "tags with spaces")]
    #[test_case(" foo, bar,  baz !all  ", true, Tags::Explicit(hset!["foo", "bar", "baz"]); "tags with !all")]
    fn parse_response_ok(s: &str, apply_to_all: bool, tags: Tags) {
        assert_eq!(Response::from_str(s), Ok(Response { apply_to_all, tags }))
    }

    #[test_case("!ibid and more tags", ParseResponseError::IbidWithOtherTags; "ibid with other tags")]
    #[test_case("!unknown", ParseResponseError::UnknownControlCode("unknown".into()); "unknown control code")]
    fn parse_response_err(s: &str, err: ParseResponseError) {
        assert_eq!(Response::from_str(s), Err(err));
    }
}
