//! Logic for sending out the "what are you doing?" pings.
//!
//! This logic is a bit convoluted since we want to support modifying tags on
//! recent pings to support the workflow of "I have added a new tag, so now I
//! want to apply it to previous pings".

use crate::{
    command::{HandlerResult, State},
    config::Config,
    models::Sample,
    response::{Response, Tags},
    VenomDialogue, VenomPool,
};
use ::time::Duration;
use log::error;
use rand::Rng;
use std::{collections::HashSet, str::FromStr, sync::Arc};
use teloxide::{
    prelude::*,
    types::{KeyboardButton, KeyboardMarkup, KeyboardRemove},
};
use tokio::time;

/// How much time to sleep at a time.
const RESOLUTION: Duration = Duration::seconds(1);

/// Does all the actual pinging. This will never return under normal circumstances.
pub async fn ping_forever(
    bot: Bot,
    chat_id: ChatId,
    dialogue: VenomDialogue,
    config: Arc<Config>,
) -> anyhow::Result<()> {
    loop {
        // TODO: Yes, I know the math doesn't strictly work out here because we might
        // get scheduled more than `RESOLUTION` later. I'll deal with that
        // later.
        time::sleep(RESOLUTION.try_into().unwrap()).await;
        // We ping once every `interval - minimum` on average, and sleep for
        // `minimum` after a successful ping, so pings are `interval`
        // apart. This also means that restarting the bot doesn't preserve the
        // minimums and will make the first ping come faster, but eh.
        let should_ping =
            rand::thread_rng().gen_bool(RESOLUTION / (config.interval - config.minimum));
        if should_ping {
            let result = send_ping(&dialogue, bot.clone(), chat_id).await;
            if let Err(err) = result {
                error!("Error sending ping: {:?}", err);
                bot.send_message(chat_id, format!("Error: {:?}", err))
                    .await?;
            }
            time::sleep(config.minimum.try_into().unwrap()).await;
        }
    }
}

/// Sends an individual ping, if the user is in a state where one should be
/// sent.
async fn send_ping(dialogue: &VenomDialogue, bot: Bot, chat_id: ChatId) -> anyhow::Result<()> {
    dialogue.update(State::Pinged).await?;
    bot.send_message(chat_id, "What are you doing?")
        .reply_markup(
            // We don't bother sending it as one_time_markup since we're going
            // to delete the keyboard anyway in the reply. This is because
            // one_time_keyboard doesn't make the keyboard go away if you
            // manually type a reply, and it also has weird sync behavior
            // across clients.
            KeyboardMarkup::new(vec![vec![KeyboardButton::new("!ibid")]]).resize_keyboard(true),
        )
        .await?;
    Ok(())
}

/// Manually triggers a dry run ping.
pub async fn handle_manual_ping(dialogue: VenomDialogue, bot: Bot, msg: Message) -> HandlerResult {
    send_ping(&dialogue, bot, msg.chat.id).await?;
    Ok(())
}

/// Invoked when the user replies to a ping (i.e., when the user sends a
/// message that isn't a command and we're in the `State::Pinged` state.
pub async fn handle_ping_response(
    dialogue: VenomDialogue,
    pool: VenomPool,
    bot: Bot,
    msg: Message,
    config: Arc<Config>,
) -> HandlerResult {
    let Some(text) = msg.text() else {
        bot.send_message(msg.chat.id, "Need actual response").await?;
        // This isn't an internal error, so we should return Ok.
        return Ok(())
    };
    let response = Response::from_str(text)?;
    let mut conn = pool.begin().await?;
    let inserted_sample = match response.tags {
        Tags::Explicit(tags) => {
            let tags: HashSet<_> = tags
                .into_iter()
                .flat_map(|tag| config.tags.normalize(&tag))
                .collect();
            let sample = Sample::insert(&mut conn, None).await?;
            sample.add_tags(&mut conn, &tags).await?;
            sample
        }
        Tags::Ibid => Sample::ibid(&mut conn, None).await?,
    };
    let tags = inserted_sample.tags(&mut conn).await?;
    conn.commit().await?;
    bot.send_message(
        msg.chat.id,
        format!(
            "Inserted sample {} with tags [{}]",
            inserted_sample.id,
            tags.into_iter()
                .map(|tag| tag.name)
                .collect::<Vec<_>>()
                .join(", ")
        ),
    )
    .reply_markup(KeyboardRemove::new())
    .await?;
    dialogue.update(State::Idle).await?;
    Ok(())
}
