//! Representation and serialization/deserialization of configuration.
//!
//! We don't allow modifying this at runtime via the Telegram interface since I
//! haven't built an interface for it and then we'd have to have a way to write
//! it back to disk.

use figment::{
    providers::{Format, Toml},
    Figment,
};
use serde::{Deserialize, Deserializer};
use std::{
    collections::{HashMap, HashSet},
    str::FromStr,
};
use thiserror::Error;
use time::Duration;

/// In-memory representation of the configuration. This is not the same as the
/// on-disk format, which is why this doesn't implement Deserialize.
#[derive(Clone, Debug, PartialEq, Eq, Deserialize)]
pub struct Config {
    /// Configuration related to tag canonicalization, etc.
    #[serde(default)]
    pub tags: TagConfig,
    /// Mean interval between pings.

    #[serde(default = "default_interval", deserialize_with = "deserialize_minutes")]
    pub interval: Duration,

    /// The minimum amount of time between pings.
    #[serde(default = "default_minimum", deserialize_with = "deserialize_minutes")]
    pub minimum: Duration,
}

#[derive(Clone, Default, Debug, PartialEq, Eq, Deserialize)]
#[serde(try_from = "HashMap<String, DiskTagConfig>")]
pub struct TagConfig {
    /// Maps from a tag to its canonical name.
    #[serde(default)]
    canonical: HashMap<String, String>,
    /// For each tag, maps to the set of tags that it implies.
    #[serde(default)]
    implications: HashMap<String, HashSet<String>>,
}

impl Config {
    /// Loads the configuration from the given figment source. This performs
    /// validation that the `Deserialize` implementation doesn't; if you don't
    /// use this, you need to call `.validate()`.
    pub fn load(figment: Figment) -> Result<Self, ParseConfigError> {
        let config: Self = figment.extract()?;
        config.validate()?;
        Ok(config)
    }

    pub fn validate(&self) -> Result<(), ParseConfigError> {
        if self.minimum >= self.interval {
            return Err(ParseConfigError::MinimumIntervalTooLarge(
                self.minimum,
                self.interval,
            ));
        }
        Ok(())
    }
}

impl FromStr for Config {
    type Err = ParseConfigError;

    /// Parse the config from the given string as TOML.
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Self::load(Figment::from(Toml::string(s)))
    }
}

impl TagConfig {
    pub fn canonicalize<'a>(&'a self, tag: &'a str) -> &'a str {
        self.canonical.get(tag).map(String::as_str).unwrap_or(tag)
    }

    pub fn implications(&self, tag: &str) -> Option<&HashSet<String>> {
        self.implications.get(self.canonicalize(tag))
    }

    /// The canonicalize version of the tag as well as its implications.
    pub fn normalize(&self, tag: &str) -> HashSet<String> {
        let mut normalized = self.implications(tag).cloned().unwrap_or_default();
        normalized.insert(self.canonicalize(tag).to_string());
        normalized
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Deserialize)]
#[serde(deny_unknown_fields)]
struct DiskTagConfig {
    /// These tag names should be canonicalized to this tag; for example,
    /// `ffxiv` might have `final fantasy xiv`, `lizard girl game`, and so
    /// on in its `alternatives`. Having a single tag name be in the
    /// alternatives for multiple tags is an error, since then we can't
    /// know which one is the 'canonical' form.
    #[serde(default)]
    pub canonicalizes_from: HashSet<String>,
    /// These tags will also imply the presence of this tag. For example,
    /// the `video games` tag might have `ffxiv` and `path of exile` in its
    /// `implied_by`. Unlike with `alternatives`, you can have a tag in
    /// multiple `implied_by` sections.
    #[serde(default)]
    pub implied_by: HashSet<String>,
}

impl TryFrom<HashMap<String, DiskTagConfig>> for TagConfig {
    type Error = ParseConfigError;

    fn try_from(tags: HashMap<String, DiskTagConfig>) -> Result<Self, Self::Error> {
        let mut canonical: HashMap<String, String> = HashMap::new();
        let mut implications: HashMap<String, HashSet<String>> = HashMap::new();

        for (tag, tag_config) in tags.iter() {
            // We manually iterate over the loop so we can bail out early if we get
            // a tag that should canonicalize to multiple things at once.
            for canonicalizes_from in tag_config.canonicalizes_from.iter() {
                if let Some(old) = canonical.insert(canonicalizes_from.clone(), tag.clone()) {
                    return Err(ParseConfigError::MultipleCanonicalForms {
                        name: canonicalizes_from.clone(),
                        first: tag.clone(),
                        second: old,
                    });
                }
            }
        }

        for tag in tags.keys() {
            if let Some(canonicalized) = canonical.get(tag) {
                return Err(ParseConfigError::TopLevelMustBeCanonical(
                    tag.clone(),
                    canonicalized.clone(),
                ));
            }
        }

        // Now, process the `implied_by` entries, making sure to use their canonical forms.
        for (tag, tag_config) in tags.iter() {
            for implied_by in &tag_config.implied_by {
                let canonicalized = canonical.get(implied_by).unwrap_or(implied_by);
                implications
                    .entry(canonicalized.to_string())
                    .or_default()
                    .insert(tag.clone());
            }
        }

        Ok(Self {
            canonical,
            implications,
        })
    }
}

fn default_interval() -> Duration {
    Duration::minutes(60)
}
fn default_minimum() -> Duration {
    Duration::minutes(20)
}

fn deserialize_minutes<'de, D>(de: D) -> Result<Duration, D::Error>
where
    D: Deserializer<'de>,
{
    i64::deserialize(de).map(Duration::minutes)
}

#[derive(Clone, Debug, PartialEq, Error)]
pub enum ParseConfigError {
    /// A tag was found in the `canonicalizes_from` section for multiple tags.
    #[error("tag `{name}` canonicalizes to both `{first}` and `{second}`")]
    MultipleCanonicalForms {
        name: String,
        first: String,
        second: String,
    },
    #[error("couldn't parse config as toml: {0}")]
    ParseError(figment::Error),
    #[error("minimum interval {0} must be less than mean interval {1}")]
    MinimumIntervalTooLarge(Duration, Duration),
    #[error("top-level tag {0} must be canonical, but was an alias for {1}")]
    TopLevelMustBeCanonical(String, String),
}

impl From<figment::Error> for ParseConfigError {
    fn from(err: figment::Error) -> Self {
        Self::ParseError(err)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use literally::hset;

    #[test]
    fn errors_on_duplicate_canonicalizes_from() {
        let config = Config::from_str(
            r#"
[tags.morningstar]
canonicalizes_from = ["venus"]
[tags.eveningstar]
canonicalizes_from = ["venus"]
"#,
        );
        assert!(config.is_err());
        assert!(config
            .unwrap_err()
            .to_string()
            .contains("canonicalizes to both"));
    }

    #[test]
    fn basic_config() -> anyhow::Result<()> {
        let config = Config::from_str(
            r#"
[tags."foo bar"]
canonicalizes_from = ["foobar"]
implied_by = ["baz"]
[tags.quux]
implied_by = ["baz"]
"#,
        )?;
        assert_eq!(config.tags.canonicalize("foobar"), "foo bar");
        assert_eq!(config.tags.normalize("foobar"), hset!["foo bar"]);
        assert_eq!(
            config.tags.implications("baz"),
            Some(&hset!["foo bar", "quux"])
        );
        assert_eq!(
            config.tags.normalize("baz"),
            hset!["foo bar", "quux", "baz"]
        );
        assert_eq!(config.tags.normalize("other thing"), hset!["other thing"]);
        Ok(())
    }

    #[test]
    fn canonicalize_then_imply() -> anyhow::Result<()> {
        let config = Config::from_str(
            r#"
[tags.canonicalized]
canonicalizes_from = ["apocryphal"]

[tags.everything]
implied_by = ["canonicalized"]
"#,
        )?;
        assert_eq!(
            config.tags.normalize("apocryphal"),
            hset!["canonicalized", "everything"],
            "implied_by should be processed after canonicalization"
        );
        Ok(())
    }

    #[test]
    fn minimum_interval_too_large() -> anyhow::Result<()> {
        let config = Config::from_str(
            r#"
minimum = 100
interval = 10
"#,
        );
        let err = config.unwrap_err();
        assert!(err.to_string().contains("must be less than mean"));
        Ok(())
    }

    #[test]
    fn canonicalize_must_be_idempotent() {
        let config = Config::from_str(
            r#"
[tags.alpha]
canonicalizes_from = ["beta"]

[tags.beta]
canonicalizes_from = ["gamma"]
"#,
        );
        let err = config.unwrap_err();
        assert!(err.to_string().contains("must be canonical"));
    }

    #[test]
    fn implied_by_uses_canonical_forms() -> anyhow::Result<()> {
        let config = Config::from_str(
            r#"
[tags.mmo]
implied_by = ["final fantasy xiv"]

[tags.ffxiv]
canonicalizes_from = ["final fantasy xiv"]
"#,
        )?;
        assert_eq!(
            config.tags.implications("ffxiv"),
            Some(&hset!["mmo"]),
            "using a non-canonical tag in the implied_by should set up an implication for the canonical version"
        );
        assert_eq!(
            config.tags.implications("final fantasy xiv"),
            Some(&hset!["mmo"]),
            "using a non-canonical tag in an implied_by should still process the implication"
        );
        Ok(())
    }
}
