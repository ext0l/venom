mod command;
mod config;
mod models;
mod ping;
mod response;

use crate::{
    command::{build_handler, Command},
    config::Config,
    ping::ping_forever,
};
use anyhow::{Context};
use clap::Parser;
use command::State;
use directories::ProjectDirs;
use figment::{
    providers::{Format, Toml},
    Figment,
};
use sqlx::{SqliteConnection, SqlitePool};
use std::{env, path::PathBuf, str::FromStr, sync::Arc};
use teloxide::{dispatching::dialogue::InMemStorage, prelude::*, utils::command::BotCommands};
use tokio::task;

// Need to use the tokio Mutex here so that we can hold the guard across `.await`s.
type VenomPool = SqlitePool;
type VenomConnection = SqliteConnection;
type VenomStorage = InMemStorage<State>;
type VenomDialogue = Dialogue<State, VenomStorage>;

pub const SAVEFILE: &str = "samples.json";

/// Like stack sampling, but for your life.
#[derive(Parser, Debug)]
#[command(author, version, about)]
struct Args {
    /// Path to the configuration file.
    #[arg(short, long, default_value_os_t = config_path(), env = "VENOM_CONFIG_PATH")]
    config_path: PathBuf,
}

async fn make_pool() -> anyhow::Result<VenomPool> {
    let database_url = env::var("DATABASE_URL").expect("must set DATABASE_URL");
    VenomPool::connect(&database_url).await.map_err(Into::into)
}

async fn run_migrations(connection: &mut VenomConnection) -> anyhow::Result<()> {
    sqlx::migrate!("./migrations").run(connection).await?;
    Ok(())
}

pub fn config_path() -> PathBuf {
    let proj_dirs = ProjectDirs::from("ai", "deifactor", "venom").unwrap();
    proj_dirs.config_dir().join("config.toml")
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    pretty_env_logger::init();
    let args = Args::parse();
    log::info!("Starting venom...");

    let pool = make_pool().await.context("failed to establish database pool")?;

    run_migrations(&mut *pool.acquire().await?).await?;

    let chat_id = ChatId(
        std::env::var("VENOM_CHAT_ID")
            .context("Failed to get VENOM_CHAT_ID environment variable")
            .and_then(|s| Ok(i64::from_str(&s)?))
            .context("Failed to parse VENOM_CHAT_ID as i64")?,
    );

    let bot = Bot::from_env();
    let storage = VenomStorage::new();
    let figment = Figment::from(Toml::file(args.config_path));
    let config: Arc<Config> = Arc::new(Config::load(figment)?);

    task::spawn(ping_forever(
        bot.clone(),
        chat_id,
        Dialogue::new(storage.clone(), chat_id),
        Arc::clone(&config),
    ));

    bot.set_my_commands(Command::bot_commands()).await?;

    Dispatcher::builder(bot.clone(), build_handler(chat_id))
        .dependencies(dptree::deps![storage, pool, config])
        .build()
        .dispatch()
        .await;
    Ok(())
}
