use std::collections::HashSet;

use crate::VenomConnection;
use anyhow::{bail, Result};

use time::OffsetDateTime;

pub type TagId = i64;
pub type SampleId = i64;

#[derive(Debug, PartialEq, Eq)]
pub struct Sample {
    pub id: SampleId,
    pub time: OffsetDateTime,
}

impl Sample {
    /// Inserts a new sample into the database. If the time is `None`, uses the current time.
    pub async fn insert(
        conn: &mut VenomConnection,
        sample_time: Option<OffsetDateTime>,
    ) -> Result<Sample> {
        let sample_time = sample_time.unwrap_or_else(OffsetDateTime::now_utc);
        let sample = sqlx::query_as!(
            Sample,
            r#"INSERT INTO samples (time) VALUES (?) RETURNING id, time as "time!: _""#,
            sample_time
        )
        .fetch_one(conn)
        .await?;
        Ok(sample)
    }

    pub async fn last_sample(conn: &mut VenomConnection) -> Result<Option<Sample>> {
        let sample = sqlx::query_as!(
            Sample,
            r#"SELECT id, time as "time!: _" FROM samples ORDER BY time DESC"#
        )
        .fetch_optional(conn)
        .await?;
        Ok(sample)
    }

    /// Inserts a ping that's the same as the previous ping. Specifically, it
    /// creates a new sample with the current timestamp and then inserts an
    /// association for each tag the previous sample had.
    pub async fn ibid(
        conn: &mut VenomConnection,
        sample_time: Option<OffsetDateTime>,
    ) -> anyhow::Result<Sample> {
        let Some(last_sample) = Sample::last_sample(conn).await? else {
            bail!("Tried to ibid with no previous sample");
        };
        let new_sample = Sample::insert(conn, sample_time).await?;

        sqlx::query!(
            r#"INSERT INTO samples_tags (sample_id, tag_id)
               SELECT ?, tag_id FROM samples_tags WHERE sample_id = ?"#,
            new_sample.id,
            last_sample.id
        )
        .execute(conn)
        .await?;
        Ok(new_sample)
    }

    /// Add the given list of tags to this sample. If the tags don't exist, they will be created.
    pub async fn add_tags(
        &self,
        conn: &mut VenomConnection,
        tag_set: &HashSet<String>,
    ) -> Result<()> {
        for tag_name in tag_set {
            Tag::ensure(&mut *conn, tag_name).await?;
            sqlx::query!(
                r#"INSERT INTO samples_tags (sample_id, tag_id)
                   SELECT ?, id FROM tags WHERE name = ?
                   ON CONFLICT DO NOTHING"#,
                self.id,
                tag_name
            )
            .execute(&mut *conn)
            .await?;
        }
        Ok(())
    }

    pub async fn tags(&self, conn: &mut VenomConnection) -> Result<HashSet<Tag>> {
        let tag_vec = sqlx::query_as!(
            Tag,
            r#"SELECT tags.id, name FROM tags
                INNER JOIN samples_tags ON samples_tags.tag_id = tags.id
                WHERE sample_id = ?"#,
            self.id
        )
        .fetch_all(conn)
        .await?;
        Ok(HashSet::from_iter(tag_vec.into_iter()))
    }

    /// Delete all tags in the database for this sample.
    pub async fn delete_tags(&self, conn: &mut VenomConnection) -> Result<()> {
        sqlx::query!("DELETE FROM samples_tags WHERE sample_id = ?", self.id)
            .execute(conn)
            .await?;
        Ok(())
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Tag {
    pub id: TagId,
    pub name: String,
}

impl Tag {
    /// If there's no tag with this name, creates one. If there is, does nothing.
    ///
    /// We don't offer a 'batch' method of this since sqlite doesn't seem to
    /// support that nicely, and the performance clearly doesn't matter.
    pub async fn ensure(conn: &mut VenomConnection, tag_name: impl AsRef<str>) -> Result<()> {
        // sqlite doesn't seem to support batch insert with defaults, and the
        // performance obviously doesn't matter here, so whatever.
        let tag_name = tag_name.as_ref();
        sqlx::query!(
            "INSERT INTO tags (name) VALUES (?) ON CONFLICT DO NOTHING",
            tag_name
        )
        .execute(conn)
        .await?;
        Ok(())
    }

    /// Count the number of times each tag has occurred between `start` and
    /// `end`, inclusive. The return value is ordered from most to least
    /// frequent.
    pub async fn count(
        conn: &mut VenomConnection,
        start: OffsetDateTime,
        end: OffsetDateTime,
    ) -> Result<Vec<(Tag, i32)>> {
        let records = sqlx::query!(
            r#"
          SELECT tags.id as "id!", tags.name, COUNT(tags.id) AS "count!: i32"
          FROM samples_tags
            INNER JOIN tags ON samples_tags.tag_id = tags.id
            INNER JOIN samples ON samples_tags.sample_id = samples.id
          WHERE samples.time >= ? AND samples.time <= ?
          GROUP BY "id!"
          ORDER BY "count!" DESC
        "#,
            start,
            end
        )
        .fetch_all(conn)
        .await?;
        Ok(records
            .into_iter()
            .map(|rec| {
                (
                    Tag {
                        id: rec.id,
                        name: rec.name,
                    },
                    rec.count,
                )
            })
            .collect())
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct SampleTag {
    pub sample_id: i32,
    pub tag_id: i32,
}

#[cfg(test)]
mod tests {
    use crate::VenomPool;

    use super::*;
    use literally::hset;
    use time::{macros::datetime, Duration};

    #[sqlx::test]
    fn ensure_succeeds_if_already_present(pool: VenomPool) -> Result<()> {
        let mut conn = pool.acquire().await?;
        Tag::ensure(&mut conn, "hello").await?;
        // using Ok() here helps out type inference
        assert_eq!(
            sqlx::query!(r#"SELECT COUNT(*) as count FROM tags"#)
                .fetch_one(&mut conn)
                .await?
                .count,
            1
        );
        Tag::ensure(&mut conn, "hello").await?;
        assert_eq!(
            sqlx::query!(r#"SELECT COUNT(*) as count FROM tags"#)
                .fetch_one(&mut conn)
                .await?
                .count,
            1,
            "inserting a tag twice should leave one tag in the db"
        );
        Ok(())
    }

    #[sqlx::test]
    fn add_tags(pool: VenomPool) -> Result<()> {
        let mut conn = pool.acquire().await?;
        let sample = Sample::insert(&mut conn, None).await?;
        sample
            .add_tags(&mut conn, &hset!["one", "two", "three"])
            .await?;
        assert_eq!(
            sample.tag_names(&mut conn).await?,
            hset!["one", "two", "three"]
        );
        sample.add_tags(&mut conn, &hset!["two", "four"]).await?;
        assert_eq!(
            sample.tag_names(&mut conn).await?,
            hset!["one", "two", "three", "four"]
        );
        Ok(())
    }

    #[sqlx::test]
    fn ibid_repeats_tags(pool: VenomPool) -> Result<()> {
        let mut conn = pool.acquire().await?;
        Sample::insert(&mut conn, None)
            .await?
            .add_tags(&mut conn, &hset!["one", "two", "three"])
            .await?;
        let prev = Sample::insert(&mut conn, None).await?;
        prev.add_tags(&mut conn, &hset!["one", "three", "four"])
            .await?;
        let next = Sample::ibid(&mut conn, None).await?;
        assert_eq!(next.id, 3);
        assert_eq!(prev.tags(&mut conn).await?, next.tags(&mut conn).await?);
        Ok(())
    }

    #[sqlx::test]
    fn ibid_fails_with_no_previous_sample(pool: VenomPool) -> Result<()> {
        let mut conn = pool.acquire().await?;
        assert!(Sample::ibid(&mut conn, None).await.is_err());
        Ok(())
    }

    fn to_names(tags: Vec<(Tag, i32)>) -> HashSet<String> {
        HashSet::from_iter(tags.into_iter().map(|(tag, _)| tag.name))
    }

    #[sqlx::test]
    fn count_tags_uses_endpoints(pool: VenomPool) -> Result<()> {
        let mut conn = pool.acquire().await?;
        let now = datetime!(2023-02-10 17:00:00 UTC);
        let earlier = now - Duration::days(1);
        Sample::insert(&mut conn, Some(now))
            .await?
            .add_tags(&mut conn, &hset!["now"])
            .await?;
        Sample::insert(&mut conn, Some(earlier))
            .await?
            .add_tags(&mut conn, &hset!["earlier"])
            .await?;
        assert_eq!(
            to_names(Tag::count(&mut conn, earlier, now).await?),
            HashSet::from(["earlier".into(), "now".into()]),
            "the tag count should include both its endpoints"
        );
        assert_eq!(
            to_names(Tag::count(&mut conn, earlier + Duration::seconds(1), now).await?),
            HashSet::from(["now".into()]),
            "the tag count should check the start"
        );
        assert_eq!(
            to_names(Tag::count(&mut conn, earlier, now - Duration::seconds(1)).await?),
            HashSet::from(["earlier".into()]),
            "the tag count should check the end"
        );
        Ok(())
    }
}
