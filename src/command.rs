//! Builds the root handler and handles the associated commands.

use std::{collections::HashSet, str::FromStr, sync::Arc};

use crate::{
    config::Config,
    models::{Sample, Tag},
    ping::{handle_manual_ping, handle_ping_response},
    response::{Response, Tags}, VenomDialogue, VenomPool,
};
use teloxide::{
    dispatching::{
        dialogue::{self, InMemStorage},
        UpdateHandler,
    },
    prelude::*,
    types::ParseMode,
    utils::command::BotCommands,
};
use time::{Duration, OffsetDateTime};

pub type HandlerResult = anyhow::Result<()>;

#[derive(Clone, Default, Debug, PartialEq, Eq)]
pub enum State {
    /// There are no outstanding pings for the user.
    #[default]
    Idle,
    /// Currently waiting for a ping response.
    Pinged,
}

#[derive(BotCommands, Clone)]
#[command(rename_rule = "lowercase", description = "Available commands:")]
pub enum Command {
    #[command(description = "display this text.")]
    Help,
    #[command(description = "initialize the bot.")]
    Start,
    #[command(description = "manually trigger a ping.")]
    ManualPing,
    #[command(description = "list all tags.")]
    ListTags,
    #[command(description = "show the most popular recent tags.")]
    Stats { days: i64 },
    #[command(description = "change the tags from the previous ping.")]
    Edit { new_text: String },
}

/// Constructs the main handler/dialogue tree for the entire bot.
pub fn build_handler(chat_id: ChatId) -> UpdateHandler<anyhow::Error> {
    use dptree::case;

    let command_handler = teloxide::filter_command::<Command, _>()
        .branch(case![Command::Help].endpoint(help))
        .branch(case![Command::ManualPing].endpoint(handle_manual_ping))
        .branch(case![Command::ListTags].endpoint(list_tags))
        .branch(case![Command::Stats { days }].endpoint(stats))
        .branch(case![Command::Edit { new_text }].endpoint(handle_edit));

    let msg_handler = Update::filter_message()
        .branch(command_handler)
        .branch(case![State::Pinged].endpoint(handle_ping_response));

    let auth_check = dptree::filter(move |dialogue: VenomDialogue| dialogue.chat_id() != chat_id)
        .endpoint(unauthorized);

    dialogue::enter::<Update, InMemStorage<State>, State, _>()
        .branch(auth_check)
        .branch(msg_handler)
}

/// Handles the `/help` command.
async fn help(bot: Bot, msg: Message) -> HandlerResult {
    bot.send_message(msg.chat.id, Command::descriptions().to_string())
        .await?;
    Ok(())
}

/// Handles the case where the user isn't the person running the bot.
async fn unauthorized(bot: Bot, msg: Message) -> HandlerResult {
    bot.send_message(msg.chat.id, "403 Forbidden").await?;
    Ok(())
}

/// Send the statistics for the past `days` days.
async fn stats(bot: Bot, msg: Message, pool: VenomPool, days: i64) -> HandlerResult {
    let now = OffsetDateTime::now_utc();
    let mut conn = pool.acquire().await?;
    let tags = Tag::count(&mut conn, now - Duration::days(days), now).await?;

    let stats_list: String = tags
        .into_iter()
        .take(10)
        .map(|(Tag { name, .. }, count)| format!("• **{name}**: {count}\n"))
        .collect();
    let days_word = if days == 1 { "day" } else { "days" };
    bot.send_message(
        msg.chat.id,
        format!("Your stats for the past {days} {days_word}:\n\n{stats_list}"),
    )
    .parse_mode(ParseMode::MarkdownV2)
    .await?;
    Ok(())
}

/// Lists all tags.
async fn list_tags(bot: Bot, msg: Message, pool: VenomPool) -> HandlerResult {
    let mut conn = pool.acquire().await?;
    let tags = sqlx::query!("SELECT name FROM tags ORDER BY id asc")
        .fetch_all(&mut conn)
        .await?;
    let tags_list: String = tags
        .into_iter()
        .map(|tag| format!("• {0}\n", tag.name))
        .collect();
    bot.send_message(msg.chat.id, tags_list).await?;
    Ok(())
}

/// Edit the previous ping.
async fn handle_edit(
    bot: Bot,
    msg: Message,
    pool: VenomPool,
    new_text: String,
    config: Arc<Config>,
) -> HandlerResult {
    let mut conn = pool.acquire().await?;
    let response = Response::from_str(&new_text)?;
    if response.apply_to_all {
        bot.send_message(msg.chat.id, "Cannot use !all with editing.")
            .await?;
        return Ok(());
    }
    let Tags::Explicit(tags) = response.tags else {
        bot.send_message(msg.chat.id, "Must use explicit tags when editing.").await?;
        return Ok(())
    };
    let Some(last_sample) = Sample::last_sample(&mut conn).await? else {
        bot.send_message(msg.chat.id, "Cannot edit a sample with no sample to edit!").await?;
        return Ok(())
    };
    last_sample.delete_tags(&mut conn).await?;
    let tags: HashSet<_> = tags
        .into_iter()
        .flat_map(|tag| config.tags.normalize(&tag))
        .collect();
    last_sample.add_tags(&mut conn, &tags).await?;
    bot.send_message(
        msg.chat.id,
        format!(
            "Edited. Sample {} now has tags [{}]",
            last_sample.id,
            Vec::from_iter(tags.into_iter()).join(", ")
        ),
    )
    .await?;
    Ok(())
}
