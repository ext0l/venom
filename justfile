set dotenv-load

# delete the dev database and remake it
remake-dev-db:
    sqlx database drop
    sqlx database create
    sqlx migrate run
