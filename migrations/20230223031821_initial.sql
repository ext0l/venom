CREATE TABLE samples (
  id INTEGER PRIMARY KEY NOT NULL,
  time DATETIME NOT NULL
);

CREATE TABLE tags (
  id INTEGER PRIMARY KEY NOT NULL,
  name TEXT NOT NULL,
  UNIQUE (name)
);

CREATE TABLE samples_tags (
  sample_id INTEGER NOT NULL,
  tag_id INTEGER NOT NULL,
  PRIMARY KEY (sample_id, tag_id),
  FOREIGN KEY(sample_id) REFERENCES samples(id),
  FOREIGN KEY(tag_id) REFERENCES tags(id)
);
